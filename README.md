# golang-echo-framework .
## This is template for Golang using Echo Framework by Hung Son - Scommerce, based on go-echo-api.g8.

Copyright (C) by Scommerce.

Latest release version: 0.1.0

### How to use

* Run the server on local

```go
go run main.go
```

* Build to run standalone

```go
go build -o main
```

* Build docker image 

```
docker build -t <IMAGE_NAME> .
```
