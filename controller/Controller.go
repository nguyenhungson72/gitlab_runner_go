package controller

import (
	"github.com/labstack/echo/v4"
)

func ApiResult(c echo.Context, status int, data interface{}) error {
	mapData := make(map[string]interface{})
	mapData["status"] = status
	mapData["data"] = data

	return c.JSON(status, mapData)
}
