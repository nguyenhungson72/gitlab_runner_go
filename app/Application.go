package app

import (
	"net/http"
	"os"
	"path"
	"path/filepath"
	"runtime"
	"strconv"
	"time"

	log "github.com/sirupsen/logrus"

	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/labstack/echo/v4"
)

const (
	defaultConfigFile = "/config/application.conf"
)

var (
	AppConfig    *HoconConfig
	echoInstance *echo.Echo
)

func initAppConfig() *HoconConfig {
	configFile := os.Getenv("APP_CONFIG")
	if configFile == "" {
		log.Infof("No environment APP_CONFIG found, fallback to [%s]", defaultConfigFile)
		_, b, _, _ := runtime.Caller(0)
		d := path.Join(path.Dir(b))
		configFile = filepath.Dir(d) + defaultConfigFile
	}
	return loadAppConfig(configFile)
}

func getEchoInstance() *echo.Echo {
	if echoInstance == nil {
		echoInstance = echo.New()

		// Define middleware for API
		AppMiddleware(echoInstance)

		// Define route for API
		Routes(echoInstance)
	}

	return echoInstance
}

func initApp() {
	// Init database connection, queue, cache
}

func Start() {
	AppConfig = initAppConfig()
	log.Infof("Get app name [%s]", AppConfig.Conf.GetString("app.name"))

	// Init echo config
	e := getEchoInstance()
	listenAddr := AppConfig.Conf.GetString("http.listen_addr", "127.0.0.1")
	listenPort := AppConfig.Conf.GetInt32("http.listen_port", 8080)
	request_timeout := AppConfig.Conf.GetInt32("api.request_timeout", 10)

	// Init App
	initApp()

	// Start server
	s := &http.Server{
		Addr:         listenAddr + ":" + strconv.Itoa(int(listenPort)),
		ReadTimeout:  time.Duration(request_timeout) * time.Second,
		WriteTimeout: time.Duration(request_timeout) * time.Second,
	}
	e.Logger.Fatal(e.StartServer(s))
}
