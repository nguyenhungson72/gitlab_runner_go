package app

import (
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

func AppMiddleware(e *echo.Echo) {
	// Client cannot send request that exceeds this size
	maxBody := AppConfig.Conf.GetString("api.body_limit", "64KiB")
	e.Use(middleware.BodyLimit(maxBody))

	// Add Logger middleware
	e.Use(middleware.Logger())
}
