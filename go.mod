module main

go 1.13

require (
	github.com/aws/aws-sdk-go v1.31.15 // indirect
	github.com/go-akka/configuration v0.0.0-20200115015912-550403a6bd87
	github.com/jinzhu/gorm v1.9.12
	github.com/labstack/echo/v4 v4.1.16
	github.com/lib/pq v1.1.1
	github.com/sirupsen/logrus v1.6.0
	google.golang.org/genproto v0.0.0-20200615140333-fd031eab31e7 // indirect
)
